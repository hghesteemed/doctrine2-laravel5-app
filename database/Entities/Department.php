<?php

namespace Entities;

/**
 * Department
 */
class Department
{
    /**
     * @var string
     */
    private $departmentname;

    /**
     * @var integer
     */
    private $departmentid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $employees;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set departmentname
     *
     * @param string $departmentname
     *
     * @return Department
     */
    public function setDepartmentname($departmentname)
    {
        $this->departmentname = $departmentname;

        return $this;
    }

    /**
     * Get departmentname
     *
     * @return string
     */
    public function getDepartmentname()
    {
        return $this->departmentname;
    }

    /**
     * Get departmentid
     *
     * @return integer
     */
    public function getDepartmentid()
    {
        return $this->departmentid;
    }

    /**
     * Add employee
     *
     * @param \Entities\Employee $employee
     *
     * @return Department
     */
    public function addEmployee(\Entities\Employee $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \Entities\Employee $employee
     */
    public function removeEmployee(\Entities\Employee $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}

