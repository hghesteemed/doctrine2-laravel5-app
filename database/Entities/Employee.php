<?php

namespace Entities;

/**
 * Employee
 */
class Employee
{
    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var integer
     */
    private $employeeid;

    /**
     * @var \Entities\Department
     */
    private $departmentid;


    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Employee
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Employee
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get employeeid
     *
     * @return integer
     */
    public function getEmployeeid()
    {
        return $this->employeeid;
    }

    /**
     * Set departmentid
     *
     * @param \Entities\Department $departmentid
     *
     * @return Employee
     */
    public function setDepartmentid(\Entities\Department $departmentid = null)
    {
        $this->departmentid = $departmentid;

        return $this;
    }

    /**
     * Get departmentid
     *
     * @return \Entities\Department
     */
    public function getDepartmentid()
    {
        return $this->departmentid;
    }
}

