insert into department (departmentname) 
values 
('Technology'),
('Finance'),
('Marketing');

insert into employee(firstname,lastname,departmentid)
values
('Chris','Heney',1),
('Caitlin','Simms',1),
('James','Murithi',1),
('Mollie','Lovelady',2),
('Michael','Flores',3);