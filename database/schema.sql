drop database if exists personnel;

create database personnel;

use personnel;

create table department(
departmentid integer not null primary  key auto_increment,
departmentname varchar(20)
);

create table employee(
employeeid integer not null primary key auto_increment,
firstname varchar(30),
lastname varchar(30),
departmentid integer,
foreign key emp_dept_fk (departmentid) references department(departmentid)
);